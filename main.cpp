#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <algorithm>
#include "logic.hpp"
#include "config.hpp"

void save(sf::RenderWindow &window, const Interface &interface)
{
	std::string input;
	sf::Font font;
	font.loadFromFile("hack.ttf");
	sf::Text text;
	text.setFont(font);
	text.setCharacterSize(10);
	text.setFillColor(sf::Color::White);
	sf::Event event;
	while(true)
	{
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::KeyPressed:
					switch(event.key.code)
					{
						case sf::Keyboard::Enter:
							if(input.size() > 0)
							{
								interface.SaveToFile(input);
							}
							return;
							break;
						case sf::Keyboard::BackSpace:
							if(input.size() > 0)
							{
								input.pop_back();
							}
							break;
						default:
							break;
					}
					break;
				case sf::Event::TextEntered:
					if(event.text.unicode >= ' ' && event.text.unicode <= '~')
					{
						input.push_back(static_cast<char>(event.text.unicode));
					}
					break;
				default:
					break;
			}
		}
		window.clear(sf::Color::Black);
		text.setString(std::string("Output filename: ") + input);
		window.draw(text);
		window.display();
	}
}

int main(int argc, char** argv)
{
	config global;
	ParseConfig(global);
	for(int i = 1; i < argc; ++i)
	{
		if(std::string(argv[i]) == "-c")
		{
			ParseConfig(global, argv[++i]);
		}
	}
	std::string file;
	for(unsigned char i = 1; i < argc; ++i)
	{
		std::string arg = argv[i];
		if(arg == "-w")
		{
			global.window.width = std::stol(argv[++i]);
		}
		else if(arg == "-h")
		{
			global.window.height = std::stol(argv[++i]);
		}
		else if(arg == "-f")
		{
			global.simulation.target = std::stol(argv[++i]);
		}
		else if(arg == "-r")
		{
			global.window.framerate = std::stol(argv[++i]);
		}
		else if(arg == "-t")
		{
			global.simulation.fade = std::stoi(argv[++i]);
		}
		else if(arg == "-x")
		{
			global.grid.width = std::stoi(argv[++i]);
		}
		else if(arg == "-y")
		{
			global.grid.height = std::stoi(argv[++i]);
		}
		else if(arg == "-i")
		{
			file = argv[++i];
		}
	}
	sf::RenderWindow window(sf::VideoMode(global.window.width, global.window.height), "Game of Threads");
	state SimState = state::paused;
	Simulation simulation(global.grid.width, global.grid.height, global.simulation.fade);
	Interface interface(simulation, global.grid.width, global.grid.height);
	window.setView(interface.getView());
	if(global.simulation.target != static_cast<unsigned long>(-1))
	{
		SimState = state::running;
		if(file.size() == 0)
		{
			simulation.randomize();
		}
	}
	if(file.size() > 0)
	{
		interface.LoadFromFile(file);
	}
	Workpool wp(simulation, std::thread::hardware_concurrency(), simulation.height);
	std::vector<uint8_t> pixels;
	window.setKeyRepeatEnabled(false);
	window.setFramerateLimit(global.window.framerate);
	while(window.isOpen() && (global.simulation.target == static_cast<unsigned long>(-1) || global.simulation.frames < global.simulation.target))
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::KeyPressed:
					if(event.key.code == sf::Keyboard::Space)
					{
						switch(SimState)
						{
							case state::paused:
								SimState = state::running;
								break;
							case state::running:
								SimState = state::paused;
								break;
							default:
								break;
						}
					}
					else if(event.key.code == sf::Keyboard::S)
					{
						SimState = state::step;
					}
					else if(event.key.code == sf::Keyboard::Escape)
					{
						window.close();
					}
					else if(event.key.code == sf::Keyboard::C)
					{
						simulation.clear();
					}
					else if(event.key.code == sf::Keyboard::R)
					{
						simulation.randomize();
					}
					else if(event.key.code == sf::Keyboard::Enter)
					{
						save(window, interface);
					}
					break;
				case sf::Event::MouseButtonPressed:
					++global.simulation.mousepresses;
					if(event.mouseButton.button == sf::Mouse::Button::Middle)
					{
						interface.new_pos = true;
					}
					break;
				case sf::Event::MouseButtonReleased:
					--global.simulation.mousepresses;
					break;
				case sf::Event::MouseWheelScrolled:
					interface.changeZoom(event.mouseWheelScroll.delta);
					window.setView(interface.getView());
				default:
					break;
			}
		}
		if(SimState != state::paused)
		{
			wp.run();
			++global.simulation.frames;
			wp.finish();
			simulation.SwitchPlanes();
		}
		if(SimState == state::step)
		{
			SimState = state::paused;
		}
		if(global.simulation.mousepresses)
		{
			interface.HandleMouse(window);
		}
		window.clear(sf::Color(128, 128, 128, 255));
		window.draw(interface.render());
		window.display();
	}
	return 0;
}
