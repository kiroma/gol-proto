#pragma once
#include <string>

struct config
{
	struct window
	{
		unsigned int width = 300;
		unsigned int height = 200;
		unsigned int framerate = 60;
	}window;
	struct grid
	{
		unsigned int width = 300;
		unsigned int height = 200;
	}grid;
	struct simulation
	{
		unsigned long target = -1;
		unsigned int frames = 0;
		unsigned char mousepresses = 0;
		unsigned char fade = 48;
	}simulation;
};

void ParseConfig(config &target, const std::string &file = "default.cfg");
