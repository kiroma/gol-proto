#include "logic.hpp"
#include <fstream>

Interface::Interface(Simulation &sim, unsigned int width, unsigned int height) : width(width), height(height), sim(sim), window(sf::Vector2f(width, height)), offscreen(sf::Vector2f(width, height)), view(sf::Vector2f(static_cast<float>(width)/2, static_cast<float>(height)/2), sf::Vector2f(width, height))
{
	offscreen_tex.create(sim.width, sim.height);
	window_tex.create(sim.width, sim.height);
	offscreen.setTexture(&offscreen_tex);
	window.setTexture(&window_tex.getTexture());
}

const sf::RectangleShape& Interface::render()
{
	sim.ToPixels(pixels);
	offscreen_tex.update(pixels.data());
	window_tex.draw(offscreen);
	window_tex.display();
	return window;
}

const sf::View& Interface::getView()
{
	return view;
}

void Interface::HandleMouse(sf::RenderWindow& window)
{
	const sf::Vector2i mouse_pos = sf::Mouse::getPosition(window);
	float mx = mouse_pos.x * zoom;
	mx *= sim.width;
	mx /= window.getSize().x;
	float my = mouse_pos.y * zoom;
	my *= sim.height;
	my /= window.getSize().y;
	if((mx + offset.x) < sim.width && (mx + offset.x) >= 0)
	{
		if((my + offset.y < sim.height) && (my + offset.y) >= 0)
		{
			if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
			{
				sim.GetPlane()[static_cast<unsigned int>(my+offset.y)*sim.width+mx+offset.x] = true;
			}
			else if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Right))
			{
				sim.GetPlane()[static_cast<unsigned int>(my+offset.y)*sim.width+mx+offset.x] = false;
			}
		}
	}
	if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Middle))
	{
		if(new_pos)
		{
			last_pos = {mx, my};
			new_pos = false;
		}
		else
		{
			view.move((last_pos.x - mx) * width/sim.width, (last_pos.y - my) * height/sim.height);
			offset += {(last_pos.x - mx) * width/sim.width, (last_pos.y - my) * height/sim.height};
			window.setView(view);
			last_pos = {mx, my};
		}
	}
}

void Interface::changeZoom(const float delta)
{
	float newZoom;
	if(delta > 0)
	{
		view.zoom(newZoom = 0.5f*delta);
	}
	else
	{
		view.zoom(newZoom = 2.0f*(-delta));
	}
	zoom *= newZoom;
	if(newZoom < 1)
	{
		offset.x += (width/2)*zoom;
		offset.y += (height/2)*zoom;
	}
	else
	{
		offset.x -= (width/2)*zoom/newZoom;
		offset.y -= (height/2)*zoom/newZoom;
	}
}

void Interface::LoadFromFile(const std::string& filename)
{
	std::ifstream ifs(filename);
	std::string input;
	for(unsigned int i = 0; ifs >> input && i < height; ++i)
	{
		for(unsigned int j = 0; j < input.size() && j < width; ++j)
		{
			sim.GetPlane()[(i*width)+j] = (input[j] == '1');
		}
	}
}
void Interface::SaveToFile(const std::string& filename) const
{
	std::ofstream ofs(filename);
	for(unsigned int i = 0; i < height; ++i)
	{
		for(unsigned int j = 0; j < width; ++j)
		{
			ofs << static_cast<char>('0' + sim.GetPlane()[(i*width)+j]);
		}
		ofs << '\n';
	}
}
