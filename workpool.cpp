#include "logic.hpp"

void Workpool::SimulationThread(unsigned int id, Simulation& sim, const size_t chunk_size)
{
	while (true)
	{
		readySemaphore.acquire();
		if (terminate)
		{
			break;
		}
		const size_t current_chunk = chunk_idx.fetch_add(1, std::memory_order_acquire);
		sim.run(chunk_size * current_chunk, chunk_size * (current_chunk + 1));
		finishedSemaphore.release();
	}
}

Workpool::Workpool(Simulation& sim, const unsigned int threads, const unsigned int size) : target(threads - 1),
																						   threads(threads), sim(sim),
																						   size(size)
{
	for (unsigned int i = 0; i < target; ++i)
	{
		tp.emplace_back(&Workpool::SimulationThread, this, i, std::ref(sim),	(size / threads));
	}
}

Workpool::~Workpool()
{
	terminate = true;
	readySemaphore.release(target);
	for (auto& a : tp)
	{
		a.join();
	}
}

void Workpool::run()
{
	start();
	sim.run((size / threads) * target, size);
}

void Workpool::start()
{
	chunk_idx.store(0, std::memory_order_release);
	readySemaphore.release(target);
}

void Workpool::finish()
{
	for(unsigned int i = 0; i < target; ++i)
	{
		finishedSemaphore.acquire();
	}
}
