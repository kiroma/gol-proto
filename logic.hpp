#pragma once
#include <string>
#include <vector>
#include <thread>
#include <SFML/Graphics.hpp>
#include <semaphore>
#include <atomic>

class Simulation
{
	const unsigned char fade;
	bool displayed = false;
	using grid = std::basic_string<bool>;
	bool plane = 0;
	grid Live, LiveAlt;
	char getNeighbours(const grid &plane, const unsigned int x, const unsigned int y) const;
public:
	const unsigned int width, height;
	Simulation(const int width, const int height, const unsigned char fade) : fade(fade), Live(width*height, false), LiveAlt(width*height, false), width(width), height(height) {}
	void randomize();
	void clear();
	void run(const unsigned int start, const unsigned int end);
	void SwitchPlanes();
	grid &GetPlane();
	void ToPixels(std::vector<uint8_t> &destination);
};

class Interface
{
	const unsigned int width, height;
	Simulation &sim;
	sf::RectangleShape window;
	sf::RectangleShape offscreen;
	sf::Texture offscreen_tex;
	sf::RenderTexture window_tex;
	sf::View view;
	float zoom = 1.0f;
	sf::Vector2f last_pos;
	sf::Vector2f offset{0.0f, 0.0f};
	std::vector<uint8_t> pixels;
public:
	bool new_pos;
	Interface(Simulation &sim, unsigned int width, unsigned int height);
	const sf::RectangleShape& render();
	const sf::View& getView();
	void HandleMouse(sf::RenderWindow& window);
	void changeZoom(const float delta);
	void LoadFromFile(const std::string& filename);
	void SaveToFile(const std::string& filename) const;
};

enum state
{
	paused,
	running,
	step
};

class Workpool
{
	std::vector<std::thread> tp;
	bool terminate = false;
	std::atomic_size_t chunk_idx{};
	std::counting_semaphore<> readySemaphore{0};
	std::counting_semaphore<> finishedSemaphore{0};
	const unsigned int target;
	const unsigned int threads;
	Simulation &sim;
	const unsigned int size;
	void SimulationThread(unsigned int id, Simulation &sim, const size_t chunk_size);
public:
	Workpool(Simulation& sim, const unsigned int threads, const unsigned int size);
	~Workpool();
	void start();
	void finish();
	void run();
};
