#include "config.hpp"
#include <fstream>

void ParseConfig(config &target, const std::string &file)
{
	std::ifstream ifs(file);
	std::string input;
	while(ifs >> input)
	{
		if(input == "window")
		{
			ifs >> input;
			if(input == "width")
			{
				ifs >> input;
				target.window.width = std::stol(input);
			}
			else if(input == "height")
			{
				ifs >> input;
				target.window.height = std::stol(input);
			}
			else if(input == "framerate")
			{
				ifs >> input;
				target.window.framerate = std::stol(input);
			}
		}
		else if(input == "grid")
		{
			ifs >> input;
			if(input == "width")
			{
				ifs >> input;
				target.grid.width = std::stol(input);
			}
			else if(input == "height")
			{
				ifs >> input;
				target.grid.height = std::stol(input);
			}
		}
		else if(input == "simulation")
		{
			ifs >> input;
			if(input == "target")
			{
				ifs >> input;
				target.simulation.target = std::stol(input);
			}
			else if(input == "fade")
			{
				ifs >> input;
				target.simulation.fade = std::stoi(input);
			}
		}
	}
}
