#include "logic.hpp"
#include <random>

char Simulation::getNeighbours(const grid &plane, const unsigned int x, const unsigned int y) const
{
	char result = 0 - plane[(y*width)+x];
	const unsigned int max_y = (y+1 == height ? y : y+1);
	const unsigned int max_x = (x+1 == width ? x : x+1);
	for(unsigned int a = (y == 0 ? 0 : y-1); a <= max_y; ++a)
	{
		for(unsigned int b = (x == 0 ? 0 : x-1); b <= max_x; ++b)
		{
			result += plane[(a*width)+b];
		}
		if(result > 3)
		{
			return result;
		}
	}
	return result;
}

void Simulation::run(const unsigned int start, const unsigned int end)
{
	grid &Plane = (plane ? LiveAlt : Live);
	grid &AltPlane = (plane ? Live : LiveAlt);
	for(unsigned int y = start; y < end; ++y)
	{
		for(unsigned int x = 0; x < width; ++x)
		{
			char neighbours = getNeighbours(Plane, x, y);
			switch(neighbours)
			{
				case 2:
					AltPlane[(y*width)+x] = Plane[(y*width)+x];
					break;
				case 3:
					AltPlane[(y*width)+x] = true;
					break;
				default:
					AltPlane[(y*width)+x] = false;
					break;
			}
		}
	}
}

void Simulation::SwitchPlanes()
{
	displayed = false;
	plane = !plane;
}

Simulation::grid& Simulation::GetPlane()
{
	return (plane ? LiveAlt : Live);
}

void Simulation::randomize()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	for(auto &a : this->GetPlane())
	{
		a = mt()%2;
	}
}

void Simulation::clear()
{
	std::fill(this->GetPlane().begin(), this->GetPlane().end(), false);
}

void Simulation::ToPixels(std::vector<uint8_t>& destination)
{
	const grid &current = (plane ? LiveAlt : Live);
	const grid &previous = (plane ? Live : LiveAlt);
	destination.resize(current.size()*4);
	for(unsigned int i = 0; i < current.size(); ++i)
	{
		if(!displayed)
		{
			if(current[i])
			{
				if(!previous[i])
				{
					destination[(i*4)] = 0;
					destination[(i*4)+1] = 255;
					destination[(i*4)+2] = 0;
					destination[(i*4)+3] = 255;
				}
				else
				{
					destination[(i*4)] = 255;
					destination[(i*4)+1] = 255;
					destination[(i*4)+2] = 255;
					destination[(i*4)+3] = fade;
				}
			}
			else
			{
				if(previous[i])
				{
					destination[(i*4)] = 255;
					destination[(i*4)+1] = 0;
					destination[(i*4)+2] = 0;
					destination[(i*4)+3] = 255;
				}
				else
				{
					destination[(i*4)] = 0;
					destination[(i*4)+1] = 0;
					destination[(i*4)+2] = 0;
					destination[(i*4)+3] = fade;
				}
			}
		}
		else
		{
			if(current[i])
			{
				destination[(i*4)] = 255;
				destination[(i*4)+1] = 255;
				destination[(i*4)+2] = 255;
				destination[(i*4)+3] = fade;
			}
			else
			{
				destination[(i*4)] = 0;
				destination[(i*4)+1] = 0;
				destination[(i*4)+2] = 0;
				destination[(i*4)+3] = fade;
			}
		}
	}
	displayed = true;
}
