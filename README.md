# Conway's Game of Life 
Originally intended to make me learn graphical programming, I got too much into the logical aspect.

## Controls
### Keyboard
- `space` - Toggle simulation pause
- `s` - Step through one frame of simulation and then pause
- `r` - Randomize all cells
- `c` - Set all cells to dead
- `escape` - Exit the program

### Mouse
- `Left Mouse Button` - Set cell to alive
- `Right Mouse Button` - Set cell to dead
- `Middle Mouse Button` - Hold to move the grid around

## Commandline Arguments
- `-w <width>` - Initial width of the window in pixels. Default is 300.
- `-h <height>` - Initial height of the window in pixels. Default is 200.
- `-x <width>` - Width of the simulation grid in cells. Default is 300.
- `-y <height>` - Height of the simulation grid in cells. Default is 200.
- `-f <frames>` - How many frames to run before exiting. Starts simulating instantly with a randomized grid. Useful for benchmarking.
- `-r <fps>` - How many frames per second to run at. Default is 60.
- `-t <transparency>` - Controls the amount of blur, accepts values between [0 - 255], values under 10 are not recommended.

### Credits
- My stupid self
- Some friends at Hopson's Community Server